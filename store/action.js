import * as types from './mutation-types'

import $request from '@/common/js/http.api.js'

import Config from '@/common/js/config.js'

// 自动登录
export const autoLogin = async function({
	commit,
	state,
	dispatch
}) {
	return new Promise((resolve, reject) => {
		uni.getStorage({
			key: Config.CATCH.USER_LOGIN_INFO,
			success: async function(res) {
				let {
					username,
					password
				} = res.data
				let result = await dispatch("startLogin", {
					username,
					password
				})
				resolve(result)
			},
			fail: (res) => {
				reject("error")
				commit(types.SET_HAS_LOGIN, false)
			}
		})
	})
}

// 获取最新的验证码
export const getLastCode = function({
	commit,
	state,
	dispatch
}) {
	let currentTime = new Date().getTime()
	commit(types.SET_LOGIN_PAGE_VALID_CODE_URL,
		`${state.HOST_URL}validCode?__sid=${state.loginPage.sid}&t=${currentTime}`)
}

// 获取字典值
export const getDict = async function({
	commit,
	state,
	dispatch
}, {
	dictType
}) {
	return new Promise((resolve, reject) => {
		$request({
			url: `a/sys/dictData/treeData?dictType=${dictType}`,
			success: (res) => {
				resolve(res.data)
			}
		})
	})
}

// 开始登录
export const startLogin = function({
	commit,
	state,
	dispatch
}, {
	username,
	password,
	validCode = '',
	__url = '',
	__sid = ''
}) {
	return new Promise((resolve, reject) => {
		commit(types.SET_LOGIN_PAGE_NEED_VALID, false)
		/* username = 'c3lzdGVt'
		password = 'YWRtaW4=' */
		// 开始登录
		$request({
			url: "a/login",
			data: {
				username,
				password,
				validCode,
				__url,
				__sid
			},
			loading: "登陆中",
			success: (res) => {
				let result = res.data
				if (result.result == "false") {
					reject("error")
					if (result.isValidCodeLogin) {
						commit(types.SET_LOGIN_PAGE_NEED_VALID, true)
						commit(types.SET_LOGIN_PAGE_SID, result.sessionid)
						dispatch("getLastCode")
						// 设置用户未登录
						commit(types.SET_SESSION_ID, '')
						commit(types.SET_HAS_LOGIN, false)
						commit(types.SET_USER, {})
					}
					return uni.showToast({
						title: result.message,
						icon: 'none'
					});
				} else {
					// 设置用户已经登录  缓存全局__sid
					commit(types.SET_SESSION_ID, result.sessionid)
					commit(types.SET_HAS_LOGIN, true)
					commit(types.SET_USER, result.user)
					commit(types.SET_LOGIN_PAGE_INPUT, {})
					uni.showToast({
						title: result.message,
						icon: 'none'
					});
					// 获取角色权限信息
					dispatch("getRoles")
					resolve("ok")
				}
			}
		})
	})

}

// 角色 权限获取
export const getRoles = async function({
			commit,
			state,
			dispatch
		}) {
			return new Promise((resolve, reject) => {
					$request({
							url: "a/authInfo",
							loading: "角色获取",
							success: (res) => {
								let result = res.data
								// 判断返回内容是否包含角色信息
								if(result.roles){
									commit(types.SET_ROLES, result.roles)
								} else {
									commit(types.SET_ROLES, [])
								}
								// 判断返回内容是否包含权限信息
								if(result.stringPermissions){
									commit(types.SET_PERMISS, result.stringPermissions)
								} else {
									commit(types.SET_PERMISS, [])
								}
							}
					})
			})
}
