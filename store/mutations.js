import * as types from './mutation-types'

export default {
	[types.SET_THEME](state, detail) {
		state.theme = detail;
	},
	[types.SET_SESSION_ID](state, detail) {
		state.__sid = detail
	},
	[types.SET_HAS_LOGIN](state, detail) {
		state.hasLogin = detail
	},
	[types.SET_USER](state, detail) {
		state.user = detail
	},
	[types.SET_LOGIN_PAGE_VALID_CODE](state, detail) {
		state.loginPage.validCodeUrl = detail
	},
	[types.SET_LOGIN_PAGE_NEED_VALID](state, detail) {
		state.loginPage.needValid = detail
	},
	[types.SET_LOGIN_PAGE_SID](state, detail) {
		state.loginPage.sid = detail
	},
	[types.SET_LOGIN_PAGE_INPUT](state, detail) {
		state.loginPage.input = detail
	},
	[types.SET_LOGIN_PAGE_VALID_CODE_URL](state, detail) {
		state.loginPage.validCodeUrl = detail
	},
	[types.SET_ROLES](state, detail) {
		state.roles = detail
	},
	[types.SET_PERMISS](state, detail) {
		state.stringPermissions = detail
	}
}
