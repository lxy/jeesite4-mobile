<p align="center">
    <img width="200" src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-41e5c718-dd0a-4478-9aab-99b867086d4b/135a3e0b-cbdc-4f4e-8a1a-4c2cfaac8271.jpg">
</p>

<p align="center">
	<a href="https://gitee.com/shuaishuai1233/jeesite4-mobile/stargazers" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=star&user=shuaishuai1233&project=jeesite4-mobile"/>
	</a>
	<a href="https://gitee.com/shuaishuai1233/jeesite4-mobile/members" target="_blank">
		<img src="https://svg.hamm.cn/gitee.svg?type=fork&user=shuaishuai1233&project=jeesite4-mobile"/>
	</a>
	<img src="https://svg.hamm.cn/badge.svg?key=Platform&value=微信小程序"/>
</p>

<div align="center">



<p>基于uni-app、colorUi、l-file，支持小程序、H5、Android和IOS</p>

```
如果觉得对你有用，随手点个 🌟 Star 🌟 支持下，这样才有持续下去的动力，谢谢！～
```

</div>

</br></br>
## 架构

<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-41e5c718-dd0a-4478-9aab-99b867086d4b/f765900c-0f3b-4e18-a928-a6bc21061501.svg"/>

* 请修改 common/js/config.js  SERVER.HOST_URL  SECRET_KEY

* 需要开启跨域

* 已支持安卓APP IOSAPP(>=V4.1.9) 小程序(https) H5

* HbuilderX 3.0.5

* 小程序最新版

* 如果登录使用Base64加密,请修改  \components\js-login

```javascript
import DesUtils from '@/common/js/des.js' 修改为 import Base64 from '@/common/js/base64.js'
let username = DesUtils.encode(formData.username, this.secretKey) 修改为 let username =Base64.encode(formData.username)
let password = DesUtils.encode(formData.password, this.secretKey) 修改为 let password = Base64.encode(formData.password)
let validCode = DesUtils.encode(formData.validCode, this.secretKey) 修改为 let validCode = Base64.encode(formData.validCode)
```


* QQ群:[852598160](https://jq.qq.com/?_wv=1027&k=wJDM5KRb)

# 更新日志

## [1.0.0] - 2021-03-23

### 新增

* jeesite4基础表单模块 
* 单行文本 多行文本 单选 多选 时间(日期选择) 树形结构单选(多选) 图片上传 文件上传
* 菜单权限(角色)管理
