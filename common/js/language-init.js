// i18n部分的配置
// 引入语言包，注意路径
import Vue from 'vue'
import Chinese from '@/common/js/locales/zh.js';
import English from '@/common/js/locales/en.js';
import Config from '@/common/js/config.js';
// VueI18n
import VueI18n from '@/common/js/vue-i18n.min.js';

// VueI18n
Vue.use(VueI18n);

// 当前语言
let currentLanguage = Config.LANGUAGE

export default new VueI18n({
	// 默认语言
	locale: currentLanguage,
	// 引入语言文件
	messages: {
		'zh': Chinese,
		'en': English,
	}
});