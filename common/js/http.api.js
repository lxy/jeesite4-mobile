// 获取配置文件
import Config from './config'

// 引入仓库
import store from '@/store/index.js'

function params(data) {
	let url = ''
	for (var k in data) {
		let value = data[k] !== undefined ? data[k] : ''
		url += `&${k}=${encodeURIComponent(value)}`
	}
	// 返回url,并取消url中的第一个&符号
	return url ? url.substring(1) : ''
}

export default function $request(para){
	if(!para.data){
		para.data = {};
	}
	if(!para || !para.url){
		return uni.showToast({
			icon: 'none',
			title: '请填写相关参数'
		});
	}
	uni.showLoading({
		title: para.loading || Config.SERVER.LOADING
	})
	let options = {}
	let extra = "__ajax=json&t=" + new Date().getTime() + "&param_deviceType=mobileApp&__sid=" + (para.data.__sid || store.state.__sid)
	// 设置请求的 header，header 中不能设置 Referer。
	options.header = para.header || Config.SERVER.HEADER
	// 请求方式
	options.method = para.method || Config.SERVER.METHOD
	// 判断请求类型
	if("GET" == options.method){
		// 开发者服务器接口地址
		options.url = Config.SERVER.HOST_URL + para.url + (para.url.indexOf('?') < 0 ? '?' : '&') + params(para.data) + '&' + extra
	} else {
		// 开发者服务器接口地址
		options.url = Config.SERVER.HOST_URL + para.url + (para.url.indexOf('?') < 0 ? '?' : '&') + extra
		// 请求的参数
		options.data = para.data || {}
	}
	// 超时时间，单位 ms
	options.timeout = para.timeout || Config.SERVER.TIMEOUT
	// 如果设为 json，会尝试对返回的数据做一次 JSON.parse
	options.dataType = para.dataType || Config.SERVER.DATA_TYPE
	// 设置响应的数据类型。合法值：text、arraybuffer
	options.responseType = para.responseType || Config.SERVER.RESPONSE_TYPE
	// 验证 ssl 证书
	options.sslVerify = para.sslVerify || Config.SERVER.SSL_VERIFY
	// 跨域请求时是否携带凭证（cookies）
	options.withCredentials = para.withCredentials || Config.SERVER.WITH_CREDENTIALS
	// DNS解析时优先使用ipv4
	options.firstIpv4 = para.firstIpv4 || Config.SERVER.FIRST_IPV4
	// 收到开发者服务器成功返回的回调函数
	options.success = async function(res) {
		// 登录失败后自动重新登录
		if(res.data.result == "login"){
			let result = await store.dispatch("autoLogin")
			if(result == 'ok'){
				$request(para)
			}
			return;
		}
		para.success(res)
	}
	// 接口调用失败的回调函数
	options.fail = () => {
		console.log("fail")
		uni.showModal({
			title: '网络错误',
			content: "是否重新加载",
			showCancel: true,
			success: function(res) {
				if (res.confirm) {
					$request(para)
				}
			}
		});
	}
	// 接口调用结束的回调函数（调用成功、失败都会执行）
	options.complete = () =>{
		uni.hideLoading()
	}
	uni.request(options)
}